import { Component, OnInit } from '@angular/core';
import { GithubService } from '../../services/github.service';
import { map } from 'rxjs/operator/map';
import { Items } from '../../intrface/items';


export interface Object {
  total_count: string;
  incomplete_results: boolean;
  items: Array<Items>;
}

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  // items: Array<Items>;
  user;
  repos;
  constructor(private _gitProfileService: GithubService) {}

  ngOnInit() {}

  searchUser() {
    this._gitProfileService.updateUser(this.user);
    this._gitProfileService.getUser().subscribe((user) => {
      // this.user = user;
      console.log(user);
    });
    this._gitProfileService.getRepos().subscribe((repos) => {
      // this.repos = repos;
      console.log(repos);
    });
  }


}
